import os

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
SINGLE_ORCA_RUN_FILE = os.path.join(PROJECT_ROOT, 'code', 'singleCoreRunORCA.py')
SINGLE_RUN_MOBKMC_FILE = os.path.join(PROJECT_ROOT, 'code', 'singleCoreRunMobKMC.py')
